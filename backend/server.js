const express = require('express')
const cors = require('cors')
const movieRouter = require('./routes/movie')

const app = express()
app.use(cors("*"));
app.use(express.json());

app.use("/movies", movieRouter);

app.listen(4000, "0.0.0.0",()=>{
    console.log("server is running on the port 4000");
})