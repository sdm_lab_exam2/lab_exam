const { Router } = require('express');
const express = require('express');
const db = require ('../db')
const utils = require('../utils')

const router = express.Router()

//Display Movie using name from Containerized MySQL
router.get("/",(request, response)=>{
    const {movieName} = request.params

    const statement = `
    select movie_title from Movie
    `

    const connection = db.openConnection()

    connection.query(statement,(error, result)=>{
        connection.end()
        console.log(result)
        if(result = null) {
            response.send("no movie found");
        }
        else(
            response.send(utils.createResult(error,result))
        )
    })
    
})

//POST --> ADD Movie data into Containerized MySQL table

router.post("/",(request, response)=>{
    const {movie_title, movie_release_date, movie_time, director_name} = request.body

    const statement = `
    insert into Movie(
        movie_title, movie_release_date, movie_time, director_name
    )
    values(
        '${movie_title}','${movie_release_date}','${movie_time}','${director_name}'
    )
    `

    const connection = db.openConnection()

    connection.query(statement,(error, result)=>{
        connection.end()
        response.send(utils.createResult(error,result))

    })
})

//Update Release_Date and Movie_Time into Containerized MySQL table
router.put("/:movie_id",(request, response)=>{
    const {movieID} = request.params

    const {movie_release_date, movie_time} = request.body

    const statement = `
    Update Movie
    set
        movie_release_date = '${movie_release_date}',
        movie_time = '${movie_time}'
    Where 
        movie_id = ${movieID}
    `

    const connection = db.openConnection()

    connection.query(statement,(error, result)=>{
        connection.end()
        response.send(utils.createResult(error,result))

    })
})

//Delete Movie from Containerized MySQL
router.delete("/:movie_id",(request, response)=>{
    const {movieID} = request.params

    const statement = `
    delete from Movie
    Where 
         movie_id = ${movieID}
    `

    const connection = db.openConnection()

    connection.query(statement,(error, result)=>{
        connection.end()
        response.send(utils.createResult(error,result))

    })
})



module.exports = router

