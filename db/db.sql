create table Movie (
    movie_id integer primary key auto_increment,
    movie_title varchar(250),
    movie_release_date date,
    movie_time time,
    director_name varchar(250)

);

insert into Movie ( 
     movie_title, movie_release_date, movie_time, director_name 
     )
 values 
    (
     "Avenger", "2019-10-20", "10:00:00", "someone" 
    );